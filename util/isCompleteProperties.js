// Function that checks if the properties of the request body matches all the expected properties for the post method
module.exports = isCompleteProperties  = (obj) => {
    const properties = ['name','songs','album','isActive'];
    const data = Object.keys(obj);

    // default result status
    let result = { status: true };

    // change the status to false if there is a missing property in the request body and add the missing property to the result object
    properties.forEach(property => {
        if(!data.includes(property)) result = { status: false, missing: property };
    })

    return result;
}