const express = require('express');
const port = 4000;
const app = express();

const artists = require('./artists');

// Custom Function
const isCompleteProperties = require('../util/isCompleteProperties')

// [SECTION] Middle Ware
app.use(express.json());


// [SECTION] Mock Database
let users = [

    {
        name: "Jojo Joestar",
        age: 25,
        username: "Jojo"
    },

    {
        name: "Dio Brando",
        age: 23,
        username: "Dio"
    },

    {
        name: "Jotaro Kujo",
        age: 28,
        username: "Jotaro"
    }

];


// [SECTION] Routes and Controllers for USERS
app.get('/users', (req, res) => {
    return res.send(users);
})

app.post('/users', (req, res) => {

    // send error message if the request body does not have a property of 'name'
    if(!req.body.hasOwnProperty("name")) {
        return res.status(400).send({
            error: 'Bad Request - missing required parameter NAME'
        })
    }

    if(!req.body.hasOwnProperty("age")) {
        return res.status(400).send({
            error: 'Bad Request - missing required parameter AGE'
        })
    }

})

// [ACTIVITY SECTION] Routes adn controllers for ARTISTS
app.get('/artists', (req, res) => {
    return res.send(artists);
})

app.post('/artists', (req, res) => {
    if(!isCompleteProperties(req.body).status || req.body.isActive === false) {
        return res.status(400).send({
            error: `Bad Request - missing required parameter ${isCompleteProperties.missing}`
        })
    }
})




app.listen(port, () => console.log(`API is now running at port ${port}`));