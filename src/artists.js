module.exports = artists = [
    {
        name: "Never Run",
        songs: [
            "Never Leave Me Behind",
            "Awake"
        ],
        album: "Friends",
        isActive: true
    },
    {
        name: "Paramore",
        songs: [
            "That's What You Get",
            "My Heart"
        ],
        album: "Riot",
        isActive: true
    },
    {
        name: "Hoodlum",
        songs: [
            "Chupoy-Chupoy",
            "Pogi Alcasid"
        ],
        album: "Tigilan Na Natin",
        isActive: true
    }
];