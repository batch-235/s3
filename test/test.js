const chai = require('chai');
const { assert } = require('chai');

// import and use chai-http to allow chai to send request to our server
const http = require('chai-http');
chai.use(http);

// Test Suite
describe("API Test Suite for users", () => {

    const url = 'http://localhost:4000';

    it("[1 - GET] Test API get users is running", (done) => {
        
        // request() method is used from chai to create an http request given to the server
        // get('/endpoint') method is used to run/access a get method route for a specific endpoint
        // end() method is used to access the response from the route supplied with an anonymous functions that receives the error and reponse
        chai.request(url)
        .get('/users')
        .end((err, res) => {

            assert.notEqual(res.text, '');
            // isDefined() is an assertion method that checks if the data is not undefined
            assert.isDefined(res);

            // done() method is used to tell chai-http when the test is
            done();
        })

    })

    it("[2 - GET] Test API get users returns an array", (done) => {

        chai.request(url)
        .get('/users')
        .end((err, res) => {
            assert.isArray(res.body);
            done();
        })

    })

    it("[3 - GET] Test API get users array first object username is Jojo", (done) => {

        chai.request(url)
        .get('/users')
        .end((err, res) => {
            assert.equal(res.body[0].username, "Jojo")
            done();
        })

    })

    it("[4 - GET] Test API get users array last object if existing", (done) => {

        chai.request(url)
        .get('/users')
        .end((err, res) => {
            console.table(res.body)
            assert.notEqual(res.body[res.body.length - 1], undefined)
            done();
        })

    })

    it("[5 - POST] Test API post users returns 400 if no name", (done) => {

        // type() method indicates the expected request body type (content-type: json)
        // send() method allows us to send a request body
        chai.request(url)
        .post('/users')
        .type("json")
        .send({
            age: 30,
            username: 'jin92'
        })
        .end((err, res) => {
            assert.equal(res.status, 400)
            done();
        })

    })

    it("[6 - POST] Test API post users returns 400 if no age", (done) => {

        chai.request(url)
        .post('/users')
        .type("json")
        .send({
            name: 'Jin Jin',
            username: 'jin92'
        })
        .end((err, res) => {
            assert.equal(res.status, 400)
            done();
        })

    })

})

// [ACtivity Test Suite]
describe("API Test suite for Artists", () => {
    const url = 'http://localhost:4000';

    it("[1] [GET /artists] Can retrieve a response", (done) => {

        chai.request(url)
        .get('/artists')
        .end((err, res) => {
            assert.isDefined(res);
            done();
        })

    })

    it("[2] [GET /artists] Check songs property of first object if it is an array", (done) => {

        chai.request(url)
        .get('/artists')
        .end((err, res) => {
            assert.isArray(res.body[0].songs);
            done();
        })

    })

    it("[3] [POST /artists] Check if there's error if no NAME", (done) => {

        chai.request(url)
        .post('/artists')
        .type('json')
        .send({
            // name: 'Test name',
            songs: [
                "Test song 1",
                "Test song 2"
            ],
            // album: "Test album",
            isActive: true
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })

    })

    it("[5] [POST /artists] Check if there's error if no SONGS", (done) => {

        chai.request(url)
        .post('/artists')
        .type('json')
        .send({
            name: 'Test name',
            // songs: [
            //     "Test song 1",
            //     "Test song 2"
            // ],
            album: "Test album",
            isActive: true
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })

    })

    it("[6] [POST /artists] Check if there's error if no ALBUM", (done) => {

        chai.request(url)
        .post('/artists')
        .type('json')
        .send({
            name: 'Test name',
            songs: [
                "Test song 1",
                "Test song 2"
            ],
            // album: "Test album",
            isActive: true
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })

    })

    it("[7] [POST /artists] Check if there's error if isActive is false", (done) => {

        chai.request(url)
        .post('/artists')
        .type('json')
        .send({
            name: 'Test name',
            songs: [
                "Test song 1",
                "Test song 2"
            ],
            album: "Test album",
            isActive: false
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })

    })
})